.. title: About
.. slug: index
.. date: 2019-12-05 12:22:25 UTC-05:00
.. tags:
.. category:
.. link:
.. description:
.. type: text


Data scientist at S&P Global Ratings in New York.

Likes bicycles, coffee, tacos, free software, the Python programming language,
and the Oxford comma.

..
   contents:: Sections
   :depth: 2


Places
======

Current
    .. class:: list-unstyled

    * Brooklyn, NY

Previous
    .. class:: list-unstyled

    * Indianapolis, IN
    * San Francisco, CA
    * Austin, TX
    * Bloomington, IN
    * Kirksville, MO
    * St. Louis, MO

Work
====

S&P Global Ratings
    Since Feb 2019: Data Scientist. New York, NY.

DataPad
    Summer 2014: Full-stack development intern. San Francisco, CA.

Anaconda Inc. (formerly Continuum Analytics)
    Summer 2013: Development intern. Austin, TX.

Publications
============

`A First Course in Network Science <https://cambridgeuniversitypress.github.io/FirstCourseNetworkScience/>`_
    Co-authored textbook published in 2020 by Cambridge University Press.

Full list of scholarly publications available `here </publications.html>`_.

Education
=========

PhD Informatics
    2019 — Indiana University, Bloomington, IN

MA Mathematics
    2011 — Indiana University, Bloomington, IN

BS Mathematics
    2009 — Truman State University, Kirksville, MO
